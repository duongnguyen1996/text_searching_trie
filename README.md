# Text_Searching_Trie
## Guide
db.csv    : là database chứa tên các địa danh

main.cpp  : chứa mã nguồn

text.txt  : nội dung đoạn văn đã lượt bỏ dấu

process.sh: dùng để build và run assignment

## Run 
$ sh process.sh

## Reference

[1] https://medium.com/basecs/trying-to-understand-tries-3ec6bede0014

[2] https://medium.com/@codingfreak/trie-data-structure-overview-and-practice-problems-2db1bad4bf49

[3] https://www.toptal.com/algorithms/needle-in-a-haystack-a-nifty-large-scale-text-search-algorithm
