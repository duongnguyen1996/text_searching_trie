#include <iostream>
#include <string>
#include <vector>
#include <fstream>

using namespace std;

#define ALPHABET_SIZE 256

struct TrieNode
{
	bool isLeaf;
	int hash_key;

	struct TrieNode *children[ALPHABET_SIZE];
};

struct TrieNode *getNewNode(void)
{
	struct TrieNode *pNode = new TrieNode;

	pNode->isLeaf = false;
	pNode->hash_key = -1;

	for (int i = 0; i < ALPHABET_SIZE; i++)
		pNode->children[i] = NULL;

	return pNode;
}

void insert(struct TrieNode *root, string key, int hash_key)
{
	struct TrieNode *pCurr = root;

	for (int i = 0; i < key.length(); i++)
	{
		int index = (int)key[i];
		if (!pCurr->children[index])
			pCurr->children[index] = getNewNode();

		pCurr = pCurr->children[index];
	}

	pCurr->isLeaf = true;
	pCurr->hash_key = hash_key;
}

void search(struct TrieNode *root, vector<string> &key, string txt)
{
	struct TrieNode *pCurr = root;

	for (int i = 0; i < txt.length(); i++)
	{
		int index = (int)txt[i];
		if (!pCurr->children[index])
		{
			if (root->children[index])
				pCurr = root->children[index];
			else
				pCurr = root;
		}
		else
		{
			pCurr = pCurr->children[index];
			if (pCurr->isLeaf)
			{
				pCurr->isLeaf = false;
				cout << key[pCurr->hash_key] << endl;
			}
		}
	}
}

void readTextFile(string filename, string &output)
{
	ifstream myFile;

	myFile.open(filename);

	if (!myFile)
	{
		cout << "Unable to open file";
		exit(1); // terminate with error
	}

	getline(myFile, output, '\0');

	myFile.close();
}

void readDB(string dbname, vector<string> &row)
{
	ifstream myDB;
	string temp;

	myDB.open(dbname);

	if (!myDB)
	{
		cout << "Unable to open file";
		exit(1); // terminate with error
	}

	while(getline(myDB, temp))
	{
		row.push_back(temp);
	}

	myDB.close();
}

int main()
{
	vector<string> key;
	string txt;

	readTextFile("text.txt", txt);
	readDB("db.csv", key);

	struct TrieNode *root = getNewNode();

	for (int i = 0; i < key.size(); i++)
	{
		insert(root, key[i], i);;
	}

	search(root, key, txt);

	return 0;
}