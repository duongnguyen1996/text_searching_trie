
// C++ implementation of search and insert
// operations on Trie
#include "stdio.h"
#include "iostream"
#include <string>

using namespace std;

const int ALPHABET_SIZE = 256;

// trie node
struct TrieNode
{
    struct TrieNode *children[ALPHABET_SIZE];

    // isEndOfWord is true if the node represents
    // end of a word
    bool isEndOfWord;
    int hash_key;
};

// Returns new trie node (initialized to NULLs)
struct TrieNode *getNode(void)
{
    struct TrieNode *pNode = new TrieNode;

    pNode->isEndOfWord = false;
    pNode->hash_key = -1;
    for (int i = 0; i < ALPHABET_SIZE; i++)
        pNode->children[i] = NULL;

    return pNode;
}

// If not present, inserts key into trie
// If the key is prefix of trie node, just
// marks leaf node
void insert(struct TrieNode *root, string key, int hash_key)
{
    struct TrieNode *pCrawl = root;

    for (int i = 0; i < key.length(); i++)
    {
        int index = (int)key[i];
        if (!pCrawl->children[index])
            pCrawl->children[index] = getNode();

        pCrawl = pCrawl->children[index];
    }

    // mark last node as leaf
    pCrawl->isEndOfWord = true;
    pCrawl->hash_key = hash_key;
}

// Returns true if key presents in trie, else
// false
void search(struct TrieNode *root, string keys[], string txt)
{
    struct TrieNode *pCrawl = root;

    for (int i = 0; i < txt.length(); i++)
    {
        int index = (int)txt[i];
        if (!pCrawl->children[index])
        {
            if (root->children[index])
                pCrawl = root->children[index];
            else
                pCrawl = root;
        }
        else
        {
            pCrawl = pCrawl->children[index];
            if (pCrawl->isEndOfWord)
            {
                pCrawl->isEndOfWord = false;
                cout << keys[pCrawl->hash_key] << endl;
            }
        }
    }
}

// Driver
int main()
{
    string keys[] = {"Hau Giang", "Hau Giangs", "Tieng Giang", "Can Tho", "Can Thos", "Dong Thap Muoi", "Dong Thap", "Ben Tre", "My Thuan"};
    string txt = "Mien , Can Thos $Can Thos @ gao =trang nuoc trong vui niem vui am no cuoc song       \
      Mien Dong Thap Muoi ruong lua  # menh mong yeu tinh yeumcs2134 th9a0m@ duyen man nong            \
      Ai qua Tien Giang xuong pha My Thuan Dong Thap                                                   \
      Ai di Hau Giang den bac Can Tho";
    int n = sizeof(keys) / sizeof(keys[0]);

    struct TrieNode *root = getNode();

    // Construct trie
    for (int i = 0; i < n; i++)
	{
		insert(root, keys[i], i);
	}
        
    // Search for different keys
    search(root, keys, txt);

    return 0;
}